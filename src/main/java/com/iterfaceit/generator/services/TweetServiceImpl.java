package com.iterfaceit.generator.services;

import com.iterfaceit.generator.dao.TweetDao;
import com.iterfaceit.generator.entities.Tweets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
@Service("tweetService")
@Transactional
public class TweetServiceImpl implements TweetService {

    @Autowired
    TweetDao dao;

    @Override
    public List<Tweets> list(int userId) {
        return dao.list(userId);
    }

    @Override
    public boolean persistTweet(Tweets tweets) {
        return dao.persistTweet(tweets);
    }
}
