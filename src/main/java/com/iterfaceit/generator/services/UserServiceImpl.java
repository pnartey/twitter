package com.iterfaceit.generator.services;

import com.iterfaceit.generator.dao.UserDao;
import com.iterfaceit.generator.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{


    @Autowired
    UserDao dao;


    @Override
    public List<Users> getUser(int userId) {
        return dao.getUser(userId);
    }

    @Override
    public List<Users> getUser(String userName) {
        return dao.getUser(userName);
    }

    @Override
    public List<Users> getUserEmail(String email) {
        return dao.getUser(email);
    }

    @Override
    public List<Users> getAuthenticate(String identifier, String password) {
        return dao.getAuthenticate(identifier,password);
    }

    @Override
    public List<Users> list() {
        return dao.list();
    }

    @Override
    public boolean persistUser(Users users) {
        return dao.persistUser(users);
    }
}
