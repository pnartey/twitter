package com.iterfaceit.generator.entities;

import com.iterfaceit.generator.controllers.TweetController;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by philip on 1/1/10.
 */
@Entity
@Table(name = "tweets", schema = "", catalog = "tweeter_db")
public class Tweets {
    private int id;
    private String message;
    private Timestamp date;
    private String formatedDate;
    private int userId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "userId")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "date")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }


    @Column(name = "date", insertable = false, updatable = false)
    public String getFormatedDate() {
        return TweetController.convertDateTime(date);
    }

    public void setFormatedDate(String formatedDate) {
        this.formatedDate=formatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tweets tweets = (Tweets) o;

        if (id != tweets.id) return false;
        if (userId != tweets.userId) return false;
        if (message != null ? !message.equals(tweets.message) : tweets.message != null) return false;
        if (date != null ? !date.equals(tweets.date) : tweets.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result+=userId;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
