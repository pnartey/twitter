package com.iterfaceit.generator.controllers;

import com.iterfaceit.generator.entities.Tweets;
import com.iterfaceit.generator.services.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
@Controller
@RequestMapping("/tweets")
@ResponseBody
@CrossOrigin
public class TweetController {

    @Autowired
    TweetService tweetService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody public List<Tweets> getList(@RequestParam("userId") int userId) {

        return tweetService.list(userId);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody public Boolean saveUser(@RequestParam("userId") int userId,
                                          @RequestParam("message") String message) {
        Tweets tweets=new Tweets();
        tweets.setUserId(userId);
        tweets.setMessage(message);



        return tweetService.persistTweet(tweets);

    }


    public static String convertDateTime(Date date){

        if(date==null)return "";
        StringBuilder result=new StringBuilder();

        LocalDateTime currentDateTime=LocalDateTime.now();


        LocalDateTime realdate=LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());

        long years=realdate.until(currentDateTime, ChronoUnit.YEARS);
        realdate=realdate.plusYears(years);
        long months=realdate.until(currentDateTime, ChronoUnit.MONTHS);
        realdate=realdate.plusMonths(months);
        long weeks=realdate.until(currentDateTime, ChronoUnit.WEEKS);
        realdate=realdate.plusWeeks(weeks);
        long days=realdate.until(currentDateTime, ChronoUnit.DAYS);
        realdate=realdate.plusDays(days);
        long hours=realdate.until(currentDateTime, ChronoUnit.HOURS);
        realdate=realdate.plusHours(hours);
        long min=realdate.until(currentDateTime, ChronoUnit.MINUTES);
        realdate=realdate.plusMinutes(min);
        long sec=realdate.until(currentDateTime, ChronoUnit.SECONDS);

        if(years!=0){
            result.append(years).append(" ").append(years == 1 ? "year" : "years");
        }

        if(months!=0){
            if(result.toString().isEmpty()) result.append(months).append(" ").append(months==1?"month":"months");
            else  {
                result.append(" and ").append(months).append(" ").append(months==1?"month":"months").append(" ago");
                return result.toString();
            }
        }

        if(weeks!=0){
            if(result.toString().isEmpty()) result.append(weeks).append(" ").append(weeks==1?"week":"weeks");
            else {
                result.append(" and ").append(weeks).append(" ").append(weeks==1?"week":"weeks").append(" ago");
                return result.toString();
            }
        }

        if(days!=0){
            if(result.toString().isEmpty()) result.append(days).append(" ").append(days==1?"day":"days");
            else {
                result.append(" and ").append(days).append(" ").append(days==1?"day":"days").append(" ago");
                return result.toString();
            }
        }

        if(hours!=0){
            if(result.toString().isEmpty()) result.append(hours).append(" ").append(hours==1?"hour":"hours");
            else {
                result.append(" and ").append(hours).append(" ").append(hours==1?"hour":"hours").append(" ago");
                return result.toString();
            }
        }

        if(min!=0){
            if(result.toString().isEmpty()) result.append(min).append(" ").append(min==1?"minute":"minutes");
            else {
                result.append(" and ").append(min).append(" ").append(min==1?"minute":"minutes").append(" ago");
                return result.toString();
            }
        }

        if(sec!=0){
            if(result.toString().isEmpty()) result.append(sec).append(" ").append(sec==1?"second":"seconds");
            else {
                result.append(" and ").append(sec).append(" ").append(sec==1?"second":"seconds").append(" ago");
                return result.toString();
            }
        }

        return  result.append(" ago").toString();
    }
}
