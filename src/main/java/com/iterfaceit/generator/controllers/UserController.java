package com.iterfaceit.generator.controllers;

import com.iterfaceit.generator.entities.Users;
import com.iterfaceit.generator.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
@ResponseBody
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value ="/userName" ,method = RequestMethod.GET)
    @ResponseBody
    public Users getUserByUserName(@RequestParam("userName") String userName) {

        List<Users> list=userService.getUser(userName);
        if(list.size()>0) return list.get(0);
        else return null;
    }

    @RequestMapping(value ="/email" ,method = RequestMethod.GET)
    @ResponseBody public Users getUserByEmail(@RequestParam("email") String email) {

        List<Users> list=userService.getUserEmail(email);
        if(list.size()>0) return list.get(0);
        else return null;
    }

    @RequestMapping(value ="/id" ,method = RequestMethod.GET)
    @ResponseBody public Users getUserById(@RequestParam("id") String id) {

        List<Users> list=userService.getUser(id);
        if(list.size()>0) return list.get(0);
        else return null;
    }

    @RequestMapping(value ="/login" ,method = RequestMethod.GET)
    @ResponseBody public Users getUserById(@RequestParam("identifier") String identifier, @RequestParam("password") String password) {

        List<Users> list=userService.getAuthenticate(identifier.trim(), password.trim());
        if(list.size()>0) return list.get(0);

//        Users users=new Users();
//        users.setUserName(identifier);
//        users.setPassword(password);
        return null;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody public List<Users> getList() {

        return userService.list();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody public Users saveUser( @RequestParam("userName") String userName,
                                          @RequestParam("password") String password,
                                          @RequestParam("firstName") String firstName,
                                          @RequestParam("lastName") String lastName,
                                          @RequestParam("email") String email,
                                        @RequestParam("phoneNumber") String phoneNumber
    ) {

        Users users=new Users();
//        users.setEmail(user.getEmail());
//        users.setUserName(user.getUserName());
//        users.setPassword(user.getPassword());
//        users.setFirstName(user.getPassword());
//        users.setLastName(user.getLastName());
//        users.setPhoneNumber(user.getPhoneNumber());
        users.setEmail(email);
        users.setUserName(userName);
        users.setPassword(password);
        users.setFirstName(firstName);
        users.setLastName(lastName);
        users.setPhoneNumber(phoneNumber);
//
        if(userService.persistUser(users)){
            return  users;
        }

        return null;

//        return true;
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody public Boolean upDateUser(@RequestParam("id") String id,
                                            @RequestParam("userName") String userName,
                                          @RequestParam("password") String password,
                                          @RequestParam("firstName") String firstName,
                                          @RequestParam("lastName") String lastName,
                                          @RequestParam("email") String email) {

        List<Users> list=userService.getUser(id);

        if(list.size()>0){
            list.get(0).setEmail(email);
            list.get(0).setUserName(userName);
            list.get(0).setPassword(password);
            list.get(0).setFirstName(firstName);
            list.get(0).setLastName(lastName);

            return userService.persistUser(list.get(0));
        }

        return false;

    }
}