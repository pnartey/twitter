package com.iterfaceit.generator.dao;

import com.iterfaceit.generator.entities.Users;

import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
public interface  UserDao {

    List<Users> getUser(int userId);
    List<Users> getUser(String userName);
    List<Users> getUserEmail(String email);
    List<Users> getAuthenticate(String identifier, String password);
    List<Users> list();

    boolean persistUser(Users users);
}
