package com.iterfaceit.generator.dao;

import com.iterfaceit.generator.entities.Tweets;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
@Repository("tweetDao")
public class TweetDaoImp extends AbstractDao implements TweetDao{
    @Override
    public List<Tweets> list(int userId) {
        Query query=getSession().createQuery(" from Tweets where userId=:userId order by id desc ");
        query.setParameter("userId", userId);
        return (List<Tweets>)query.list();
    }

    @Override
    public boolean persistTweet(Tweets tweets) {
        try{
           persist(tweets);
            return true;
        }
        catch (Exception e){

            return false;
        }
    }
}
