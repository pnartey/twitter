package com.iterfaceit.generator.dao;

import com.iterfaceit.generator.entities.Tweets;

import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
public interface TweetDao {

    List<Tweets> list(int userId);

    boolean persistTweet(Tweets tweets);
}
