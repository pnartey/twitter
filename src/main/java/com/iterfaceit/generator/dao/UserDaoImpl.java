package com.iterfaceit.generator.dao;

import com.iterfaceit.generator.entities.Users;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by philip on 1/1/10.
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {
    @Override
    public List<Users> getUser(int id) {
        Query query=getSession().createQuery(" from Users where id=:id");
        query.setParameter("id", id);
        return (List<Users>)query.list();
    }

    @Override
    public List<Users> getUser(String userName) {
        Query query=getSession().createQuery(" from Users where userName=:userName");
        query.setParameter("userName", userName);
        return (List<Users>)query.list();
    }

    @Override
    public List<Users> getUserEmail(String email) {
        Query query=getSession().createQuery(" from Users where email=:email");
        query.setParameter("email", email);
        return (List<Users>)query.list();
    }

    @Override
    public List<Users> getAuthenticate(String identifier, String password) {
        Query query=getSession().createQuery(" from Users where (userName=:identifier or email=:identifier or phoneNumber=:identifier) and password=:password");
        query.setParameter("identifier", identifier);
        query.setParameter("password", password);
        return (List<Users>)query.list();
    }

    @Override
    public List<Users> list() {
        Query query=getSession().createQuery(" from Users ");
        return (List<Users>)query.list();
    }

    @Override
    public boolean persistUser(Users users) {
        try{
            persist(users);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
